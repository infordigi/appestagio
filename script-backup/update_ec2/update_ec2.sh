#!/bin/bash

#Updating system in aws-EC2
echo "[+] update system AMI AWS_EC2"
sudo apt update 
sudo apt upgrade -y
echo "[+] install utils packages"
sudo apt install -y curl git net-tools vim wget iputils-ping traceroute vim-gtk3
echo "[+] python"
sudo apt install python -y
echo "[+] awscli"
sudo apt install awscli -y
echo "[+] Docker CE (Community Edition)"
sudo apt install -y apt-transport-https ca-certificates software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
sudo apt install -y docker-ce
sudo systemctl enable docker
sudo usermod -aG docker $USER
sudo chown $USER:$USER /usr/lib/docker
sudo chown $USER:docker /var/run/docker.sock
echo
echo "Download image hello-world"
sleep 2
docker pull hello-world
echo
echo "[+] Docker Compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo chown $USER /usr/local/bin/docker-compose
echo
echo "Your docker-compose version"
sleep 4
docker-compose --version
echo
echo "[+] nvm for nodeJS"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
echo
echo "Setting the environment variables"
sleep 2
alias nvmnodejs='export NVM_DIR="${XDG_CONFIG_HOME/:-$HOME/.}nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm'
echo
echo "Setting source ~/.bashrc"
sleep 2