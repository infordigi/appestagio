#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import commands
import subprocess

# Listar arquivos do diretório
list = commands.getoutput("sudo sh -c 'du -d1 -h /var/lib/docker/containers | sort -h | tail -2 | head -1'").split("/")

# Limpar arquivos do diretório
cleaner = os.system("sudo sh -c 'cat /dev/null > /var/lib/docker/containers/"+list[-1]+"/"+list[-1]+"-json.log'")
