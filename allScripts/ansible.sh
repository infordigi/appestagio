#!/bin/bash

# Instalação do Ansible
read -p "Do you want update your system? [y/n]: " update
if [ "$update" == "y" ]; then
  echo "Update system..."
  sleep 1
  sudo apt update
  echo
fi
echo "Add repository's program..."
sleep 1
sudo apt-add-repository ppa:ansible/ansible
echo "Update system..."
sleep 1
sudo apt update
echo "Installing Ansible..."
sleep 1
sudo apt install -y ansible
echo