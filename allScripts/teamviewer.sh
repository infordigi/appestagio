#!bin/bash

# Instalação do Team Viewer
read -p "Do you want update your system? [y/n] " update
if [ "$update" == "y" ]; then
  echo "Updatting your system..."
  sleep 2
  sudo apt update
  echo
fi
echo "Add repository's program"
sleep 2
sudo sh -c "echo 'deb http://linux.teamviewer.com/deb stable main' >> /etc/apt/sources.list.d/teamviewer.list"
sudo sh -c "echo 'deb http://linux.teamviewer.com/deb preview main' >> /etc/apt/sources.list.d/teamviewer.list"
echo "Add repository's key"
sleep 2
wget -q https://download.teamviewer.com/download/linux/signature/TeamViewer2017.asc -O- | sudo apt-key add -
echo "Update system"
sleep 1
sudo apt update
echo "Installing team Viewer..."
sleep 2
sudo apt install -y teamviewer
echo
