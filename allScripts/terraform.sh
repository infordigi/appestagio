#!/bin/bash

# Instalação do terraform
echo "Terraform installation"
read -p "Do you want update your system? (y/n): " update
if [ "$update" == "y" ]; then
	echo "Updatting system..."
	sleep 3
	sudo apt update
	echo
fi
echo "Finding for unzip!!!"
sleep 2
if [ ! unzip -h 2&> /dev/null ]; then
	echo "Aborting - unzip not installed and required"
	exit 1
fi
echo "Downloading terraform..."
sleep 2
echo "Unzip terraform..."
sudo snap install terraform
sleep 2
echo "Your terraform version"
sleep 3
terraform --version
echo
