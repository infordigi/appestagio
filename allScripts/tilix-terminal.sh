#!/bin/bash

#Tilix terminal installation
echo "Tilix terminal installtion"
read -p "Do you want to update sytem? (y/n): " update
if [ "$update" == "y" ]; then
	echo "Updating system..."
	sleep 3
	sudo apt update
	echo
fi
echo "Install repository ppa..."
sleep 2
sudo add-apt-repository ppa:webupd8team/terminix
echo
echo "Updating repository ppa..."
sleep 2
sudo apt update
echo
echo "Install Tilix terminal..."
sleep 2
sudo apt install -y tilix
echo
