#!/bin/bash

# Default installation
echo "Default installation"
echo "Set sudo password for $USER"
sudo apt update && sudo apt upgrade -y
echo

# INSTALAÇÃO DO vim, cURL, wget, iputils-ping, traceroute and net-tools
echo "Installing vim, cURL, wget and iputils-ping and net-tools..."
sleep 3
sudo apt install -y curl wget iputils-ping net-tools traceroute build-essentials vim terminator libgtkd-3-0 python
echo

## Git installation
echo "Git installation"
read -p "Do you want to install? (y/n): " s1

if [ "$s1" == "y"  ]; then   
    ./git.sh  
fi

## NodeJS installation (Next installation NVM)
echo "NodeJS installation"
read -p "Do you want to install? (y/n): " nodejs

if [ "$nodejs" == "y"  ]; then
    ./nodejs.sh    
fi

## NVM and NodeJS installation
echo "NVM and NodeJS installation"
read -p "Do you want to install? (y/n): " s2

if [ "$s2" == "y"  ]; then
    ./nvm-nodejs.sh    
fi

## VS Code installation
echo "VS Code installation"
read -p "Do you want to install? (y/n): " s3

if [ "$s3" == "y"  ]; then
    ./vscode.sh    
fi

## Terraform installation
echo "Terraform installation"
read -p "Do you want to install? (y/n): " s4

if [ "$s4" == "y" ]; then
    ./terraform.sh
fi

## Docker installation
echo "Docker installation"
read -p "Do you want to install? (y/n): " s5

if [ "$s5" == "y"  ]; then
    ./docker.sh    
fi

## Docker compose installation
echo "Docker compose installation"
read -p "Do you want to install? (y/n): " s6

if [ "$s6" == "y"  ]; then
    ./docker-compose.sh    
fi

## AWS CLI installation
echo "AWS CLI installation"
read -p "Do you want to install? (y/n): " s7

if [ "$s7" == "y"  ]; then
    ./awscli.sh    
fi

## JDK11 installation
echo "JDK11 installation"
read -p "Do you want to install? (y/n): " s8

if [ "$s8" == "y"  ]; then
    ./openjdk11.sh    
fi

## VirtualBox installation
echo "VirtuaBox 6 installation"
read -p "Do you want to install? (y/n): " s9

if [ "$s9" == "y"  ]; then
    ./virtualbox.sh    
fi

## Yarn installation
echo "Yarn installation"
read -p "Do you want to install? (y/n): " s10

if [ "$s10" == "y"  ]; then
    ./yarn.sh    
fi

## Android Studio IDE installation
echo "Android Studio IDE installation"
read -p "Do you want to install? (y/n): " s11

if [ "$s11" == "y"  ]; then
    ./androidstudio.sh    
fi

## Tilix terminal installation
echo "Tilix terminal installation"
read -p "Do you want to install? (y/n): " s12

if [ "$s12" == "y"  ]; then   
    ./tilix-terminal.sh  
fi

## Ansible installation
echo "Ansible installation"
read -p "Do you want to install? (y/n): " s13

if [ "$s13" == "y"  ]; then   
    ./ansible.sh  
fi

## FileZilla installation
echo "FileZilla installation"
read -p "Do you want to install? (y/n): " s14

if [ "$s14" == "y"  ]; then
    ./filezilla.sh  
fi

## Insomnia installation
echo "Insomnia installation"
read -p "Do you want to install? (y/n): " s15

if [ "$s15" == "y"  ]; then   
    ./insomnia.sh  
fi

## KdenLive installation
echo "KdenLive installation"
read -p "Do you want to install? (y/n): " s16

if [ "$s16" == "y"  ]; then   
    ./kdenlive.sh  
fi

## Simple Screen Record installation
echo "Simple Screen Record installation"
read -p "Do you want to install? (y/n): " s17

if [ "$s17" == "y"  ]; then   
    ./simplescreenrecord.sh  
fi

## Snap installation
echo "Snap installation"
read -p "Do you want to install? (y/n): " s18

if [ "$s18" == "y"  ]; then   
    ./snap.sh  
fi

## Team Viewer installation
echo "Team Viewer installation"
read -p "Do you want to install? (y/n): " s19

if [ "$s19" == "y"  ]; then   
    ./teamviewer.sh  
fi

## Vagrant installation
echo "Vagrant installation"
read -p "Do you want to install? (y/n): " s20

if [ "$s20" == "y"  ]; then   
    ./vagrant.sh  
fi

echo "Finished"
echo
echo "Close and reopen your terminal to apply!"
echo
echo "Good Job!!!"
echo
echo "Developed by DevPresto :-)"
