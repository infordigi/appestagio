#!/bin/bash

# Instalação do KdenLive
read -p "Do you want update system? [y/n] " update
if [ "$update" == "y" ]; then
  echo "Update system..."
  sleep 1
  sudo apt update
  echo
fi
echo "Add repository's program..."
sleep 1
add-apt-repository ppa:kdenlive/kdenlive-stable
echo "Update system..."
sleep 1
sudo apt update
echo "Installing KdenLive..."
sleep 1
sudo apt install -y kdenlive
echo