#!/bin/bash

#INSTALAÇÃO DO JDK8
echo "zulu-openjdk8 installation"
read -p "Do you want to update system? (y/n): " update
if [ "$update" == "y" ]; then
    echo "Updatting system..."
    sleep 3
    sudo apt update
    echo
fi
echo "Download openjdk11..."
sleep 2
sudo apt install -y openjdk-11-jdk
echo

echo "Your java version:"
sudo java -version
sleep 3
echo

echo "Set the JAVA_HOME environment variable"
echo "Use: 'sudo vim /etc/environment'"
echo 'Set JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"'
echo
