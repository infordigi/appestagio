#!/bin/bash

# Instalação do Insomnia
echo "Insomnia installation"
read -p "Do you want to update system: [y/n]: " update
if [  "$update" == "y" ]; then
  echo "Updatting your system..."
  sleep 3
  sudo apt update
  echo
fi
echo "Installing Insomnia"
echo "Add to sources"
sleep 2
echo "deb https://dl.bintray.com/getinsomnia/Insomnia /" | sudo tee -a /etc/apt/sources.list.d/insomnia.list
echo
echo "Add public key"
sleep 2
wget --quiet -O - https://insomnia.rest/keys/debian-public.key.asc | sudo apt-key add -
echo
echo "Refresh repository and install Insomnia"
sleep 2
sudo apt update
sudo apt install -y insomnia
echo